package model;

import com.google.gson.annotations.SerializedName;

public class Product {
	@SerializedName("rank")
	private int rank;
	
	@SerializedName("url")
	private String url;
	
	public Product() {
		this(0, "https://www.google.com");
	}
	
	public Product(int rank, String url) {
		setRank(rank);
		setUrl(url);
	}
	
	public int getRank() {
		return rank;
	} 
	
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return "{ " + "\"rank\" : " + rank + ", \"url\" : \"" + url + "\"";
	}
}
