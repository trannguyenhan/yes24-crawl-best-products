package demo;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

import model.Product;

public class Demo {
	public static void main(String[] args) throws IOException {
		List<Product> listProducts = new ArrayList<Product>();
		Document doc = Jsoup.connect("https://www.yes24.vn/san-pham-ban-chay").get();
		
		Elements list = doc.getElementsByClass("th-product-item");
		for(Element e: list) {
			int rank = Integer.parseInt(e.getElementsByClass("best-number").get(0).html());
			String url = e.getElementsByTag("a").attr("href");
			
			Product product = new Product(rank, url);
			listProducts.add(product);
		}
		
		listProducts.forEach(e -> System.out.println(e));
		
		Gson gson = new Gson();
		gson.toJson(listProducts, new FileWriter("result/output.json"));
	}
}
